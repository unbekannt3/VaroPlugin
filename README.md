## Varo Plugin by Cuuky 
<img src="http://185.194.142.45/Bilder/Varo/thumbnail.png">

Hey, ich habe viel an dem Plugin gearbeitet, es würde mich freuen, wenn du mir die Credits dafür lassen könntest.
Das Plugin läuft universal auf den Versionen 1.7 - 1.15.

**Codezeilen:** <br>
Zeilen: 26.943 <br>
Stand: 08.02.2020 <br>
Version: 2.2.6

**Libraries:** 
- http://www.mediafire.com/file/e6xl995rgt5zvmr/varo_lib.zip/file
- https://getbukkit.org/download/spigot

Bitte die <a href='https://github.com/CuukyOfficial/VaroPlugin/blob/master/CONTRIBUTING.md'>Richtlinien zum Programmieren</a> beachten 

Spigot-Seite mit Docs etc.: https://www.spigotmc.org/resources/71075/ <br>
Donation: https://www.tipeeestream.com/cuuky/donation

Vielen Dank & happy coding!
